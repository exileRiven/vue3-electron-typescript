'use strict'
import Datastore from 'nedb-promises'
import path from 'path'

const app = require('electron').app

class DataDBClass {

  // 确定数据文件的目录位置
  filePath = this.getFilePath

  filename: string = 'save0.db'

  _db: Datastore = this.BackDatastore

  get getFilePath () {
    const dbPath = process.env.NODE_ENV === 'development'
      ? '../../../../UserData/'
      : '../UserData/'
    const filePath = path.join(app.getPath('exe'), dbPath)
    return filePath
  }

  setFileName = (filename: string) => {
    this.filename = filename
    this.refreshDB()
  }

  get getFileName () {
    return this.filename
  }

  get BackDatastore () {
    return new Datastore({
      autoload: true,
      filename: this.filePath + this.getFileName,
      timestampData: true
    })
  }

  refreshDB() {
    this._db = this.BackDatastore
  }
}

export default new DataDBClass()
