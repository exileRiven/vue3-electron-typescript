import { remote } from 'electron'

export default {
  install (app: any): void {
    app.config.globalProperties.$remote = remote
  }
}
