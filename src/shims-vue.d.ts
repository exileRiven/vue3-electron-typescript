/* eslint-disable */
declare module '*.vue' {
  import type { DefineComponent } from 'vue'
  const component: DefineComponent<{}, {}, any>
  export default component
}

// 对vue进行类型补充说明
declare module '@vue/runtime-core' {
  interface ComponentCustomProperties {
    // electron: remote
    $remote: any
  }
}

// declare module '@vue/runtime-core' {
//   interface ComponentCustomProperties {
//     $remote: any;
//   }
// }
