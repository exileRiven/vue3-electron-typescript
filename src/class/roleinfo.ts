class RoleInfo {
  name!: string
  atk!: number
  def!: number
  hp!: number
  mp!: number
}

export default RoleInfo
