import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
// import remote from './plugin/electron-remote'

const app = createApp(App)

app.use(router).mount('#app')
