# demo：Vue3 + Electron + TypeScript 模板

## 依赖安装

```
yarn install
```

### 编译运行
```
yarn serve
```

### 编译打包
```
yarn build
```

### 代码格式修复
```
yarn lint
```


# 构建方法

## Step1 使用 vue 最新脚手架实例化项目

``` bash
## 安装 vue 脚手架（最新）
npm install @vue/cli

# 实例化 vue 项目
vue init vue3-electron-ts
## 配置项目选项：vue3、babel、ts、node-sass、router、eslint
# 进入项目目录（你自己的项目目录下，对应你上面创建的目录名称）
cd vue3-electron-ts
```

**注：这样我们的 vue 项目就创建好了**

## Step2 安装 electron-builder 构建插件

``` bash
## 安装插件
# 设置 yarn 安装源为淘宝源
yarn config set electron_mirror https://cdn.npm.taobao.org/dist/electron/
# 安装 electron-builder 插件
vue add electron-builder
## 安装时选择你需要的 electron 版本
# 如：v11.0.0
```

**注：可能你换源了还是很慢，耐心等待，曙光会到来的**

## Step3 运行并编译

``` bash
## 运行项目
yarn electron:serve
# 99%的概率率会出现
INFO  Launching Electron...
Failed to fetch extension, trying 4 more times
Failed to fetch extension, trying 3 more times
Failed to fetch extension, trying 2 more times
# 解决方法，找到 /src 目录下的 background.ts 文件
# 注释以下代码（vscode快捷键 ctrl + /）
if (isDevelopment && !process.env.IS_TEST) {
  // Install Vue Devtools
  try {
    await installExtension(VUEJS_DEVTOOLS)
  } catch (e) {
    console.error('Vue Devtools failed to install:', e.toString())
  }
}
## 重新运行，okay

## 编译项目
yarn electron:build
# 查看编译之后的项目
# 打包之后在项目根目录下的 dist_electron/win-unpacked 目录下有对应的 xxx.exe 文件，双击即可在 windows 环境下运行
```

# 构建历程

**给读者的话：**

在构建 electron-vue 项目的时候其实遇到了很多的坑，为了构建这个模板大概花费了一整天（24h）的时间，其间遇到的问题数不胜数，但是为了完美地构建这个项目，还是挺过来了，而且是我在**无意间**发现这种构建能实现，不会出问题，后面会把遇到的重要的问题排出来，大家来对号入座一下。

## 问题1：使用 simulatedgreg/electron-cli 脚手架创建项目版本过低不支持 Vue3

``` bash
## 版本过老
vue init simulatedgreg/electron-cli your-project
# 上述构建方式只能够创建 electron-vue 项目，其中 vue 版本为 2.x、electron 版本为 2.x.x、不支持ts，并不符合我在构建此项目（vue3、ts）的预期
```

## 问题二：使用 vue + electron 方式构建导致打包后应用白屏什么也不显示

**注：问题二描述的是项目跑 `(npm run electron:serve)` 起来没问题，但是打包成exe文件后应用白屏**

**1、方式有误**

``` bash
## 光写下面，我忘了上面这个方式有误要写什么了，咳咳，假装这里有内容
```

**2^、包管理器问题**

``` bash
## 其实这个我得点名一下网络上所有的构建方法
# 网上几乎所有的方法都是使用 vue3 + electron 方法构建项目
# 总体来说前期配置几乎完全一样
# 网络上流传的分三种
# 1、手动替换 electron-vue 的 js 为 ts（方法复杂，不支持vue3）
# 2、vue + electron，使用包管理器安装 electron-builder（打包后白屏）
# 3、vue + electron，使用 vue add 安装 electron-builder 插件（打包后白屏）

# 上述方法中的2、3可以归为一类（个人感觉 vue add 安装其实还是使用包管理器安装的）

## 本项目的构建方式是第二种，但是为什么又能够使用呢？
# 其实如果网络状态够好的话有足够的时间成本来试错，但是由于国内网络问题，导致我们安装 electron 插件的时候异常缓慢，即使我们换了淘宝源，问题也依旧存在，唯二的解决方法之一是把对应的 electron 包放在包管理器的缓存里，但是 yarn 包管理器的缓存我还没找到在哪里，因此是当天晚上下载，第二天起来验收的，万幸中间没有断开连接。

# 回到问题，为什么能够使用，我在查阅资料的时候发现有的人用的 npm， 有的人用 yarn，本人以前是一直用的 cnpm(npm)，所以在构建这个项目的时候一直使用 cnpm，理所当然的，会一直出问题，直到后面我脑子一抽更新了vue/cli，换成了 yarn 包管理器，本来我是没打算项目能编译成功的，但是无意间发现居然能行，于是在撰写本文的时候又新建了一个项目，编译之后发现能没问题。
```

## 问题3：使用 nedb[-promises] 数据库无法实现读写